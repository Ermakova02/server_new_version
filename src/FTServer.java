public class FTServer {

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Usage: java FTServer [port_number]");
            return;
        }
        Server srv = new Server(Integer.valueOf(args[0]));
        srv.start();
    }
}
